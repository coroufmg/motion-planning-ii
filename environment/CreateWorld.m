%Universidade Federal de Minas Gerais - 2016/2
%Planejamento de Movimento de Robos II
%Aluno: Adriano Martins da Costa Rezende
%Professor: Guilherme Pereira


function [w_s, Ob_number, Objetos] = CreateWorld()

    %w_s - vetor com os limites do mundo
    %Ob_number - numero de obstaculos
    %Objetos - vetor de estruturas objetos

    %Gera cenario com um obstaculo
    w_s = input('Define the world size in the form [xl xr yd yu]: ');
%     w_s = [-2 2 -1 1]; %default

    Ob_number = input('Define the number of obstacles: ');
%     Ob_number = 4; %default

    figure(1)
    plot(w_s([2 1 1 2 2]),w_s([4 4 3 3 4]),'k-','LineWidth',2)
    axis equal

    %Obtencao dos vertices dos poligonos obstaculos
    objeto = struct('vertices',0,'n',0);
    Objetos = [];
    for k = 1:1:Ob_number
        hold on;
        title(sprintf('Insert the vertices of obstacle number %d - (right click to close)',k))
        O_v = []; %vertices de um obstaculo
        button = 0;
        while button ~= 3 %enquanto o botao direito nao for apertado
            [x,y,button] = ginput(1);
            if(button ~= 1);
                if(length(O_v(:,1))>1)
                    plot([O_v(end,1) O_v(1,1)],[O_v(end,2) O_v(1,2)],'r-','LineWidth',1)
                end
                break;
            end;
            plot(x,y,'ro','LineWidth',2)
            O_v = [O_v; x,y];
            if(length(O_v(:,1))>1)
                plot([O_v(end-1,1) O_v(end,1)],[O_v(end-1,2) O_v(end,2)],'r-','LineWidth',1)
            end

        end
        title('')
        hold off
        eval(sprintf('O_v%d = O_v;',k))
        objeto = struct('vertices',O_v,'n',length(O_v(:,1)));
        Objetos = [Objetos objeto];
    end
    
end %function
